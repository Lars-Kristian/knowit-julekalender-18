public class Day6
{
    public static long Compute()
    {
        long result = 0;

        long cacheSource = 90;
        int cacheZeroCount = 0;
        int cacheDigitCount = 2;

        for (long i = 99; i <= 18100000; i++)
        {
            long source = i;

            if (source < cacheSource + 10)
            {
                if (cacheZeroCount * 2 > cacheDigitCount)
                    result += i;

                continue;
            }

            int zeroCount = 0;
            int digitCount = 1;

            while (source > 9)
            {
                if (source % 10 == 0)
                    zeroCount += 1;

                digitCount += 1;
                source /= 10;
            }

            if (zeroCount * 2 > digitCount)
                result += i;
            
            cacheZeroCount = zeroCount - 1;
            cacheDigitCount = digitCount;
        }

        return result;
    }

}
