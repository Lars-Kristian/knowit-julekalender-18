
using System;
using System.Collections.Generic;
using System.Diagnostics;

public class Day3
{

    public static long Compute()
    {
        var maxFactor = (long)(4294967296 / Math.Pow(2, 23));
        
        var factors = GetPossibleFactors(maxFactor);

        long result = 0;
        ComputeRecursive(ref result, 0, factors);
        
        return result;
    }

    private static void ComputeRecursive(ref long result, int factorIndex, List<int> factors, int level = 0, long value = 1)
    {
        var count = factors.Count;
        for (var i = factorIndex; i < count; i++)
        {
            var next = factors[i] * value;

            if (next > 4294967296){
                return;
            } 

            if (level == 23)
            {
                result += 1;
                continue;
            }

            ComputeRecursive(ref result, i, factors, level + 1, next);
        }
    }

    private static List<int> GetPossibleFactors(long max)
    {
        var result = new List<int>();

        result.Add(2);

        for (var i = 3; i < max; i += 2)
        {
            if (IsPrime(i))
            {
                result.Add(i);
            }
        }

        return result;
    }

    public static bool IsPrime(int number)
    {
        if (number <= 1) return false;
        if (number == 2) return true;
        if (number % 2 == 0) return false;

        var boundary = (int)Math.Floor(Math.Sqrt(number));

        for (int i = 3; i <= boundary; i += 2)
            if (number % i == 0)
                return false;

        return true;
    }
}