using System;
using System.Collections.Generic;
using System.IO;

public class Day10
{
    public static int Compute()
    {
        string fileInput = "";
        try
        {
            using (StreamReader sr = new StreamReader("Day10/input.txt"))
            {
                fileInput = sr.ReadToEnd();
            }
        }
        catch (Exception) { }


        var stack = new Stack<int>();
        var skip = false;

        foreach (var c in fileInput)
        {
            if (skip)
            {
                if (c == '\n') skip = false;
                continue;
            }

            switch (c)
            {
                case ':' : Command1(stack);  break;
                case '|' : Command2(stack);  break;
                case '\'': Command3(stack);  break;
                case '.' : Command4(stack);  break;
                case '_' : Command5(stack);  break;
                case '/' : Command6(stack);  break;
                case 'i' : Command7(stack);  break;
                case '\\': Command8(stack);  break;
                case '*' : Command9(stack);  break;
                case ']' : Command10(stack); break;
                case '[' : Command11(stack); break;
                case '~' : Command12(stack); break;
                case 'K' : skip = true;      break;
                case ' ' : stack.Push(31);   break;
            }
        }

        return stack.Pop();
    }

    private static void Command1(Stack<int> stack)
    {
        var result = 0;

        while (stack.Count > 0)
        {
            result += stack.Pop();
        }

        stack.Push(result);
    }

    private static void Command2(Stack<int> stack)
    {
        stack.Push(3);
    }

    private static void Command3(Stack<int> stack)
    {
        stack.Push(stack.Pop() + stack.Pop());
    }

    private static void Command4(Stack<int> stack)
    {
        var a = stack.Pop();
        var b = stack.Pop();
        stack.Push(a - b);
        stack.Push(b - a);
    }

    private static void Command5(Stack<int> stack)
    {
        var a = stack.Pop();
        var b = stack.Pop();
        stack.Push(a * b);
        stack.Push(a);
    }

    private static void Command6(Stack<int> stack)
    {
        stack.Pop();
    }

    private static void Command7(Stack<int> stack)
    {
        stack.Push(stack.Peek());
    }

    private static void Command8(Stack<int> stack)
    {
        stack.Push(stack.Pop() + 1);
    }

    private static void Command9(Stack<int> stack)
    {
        double tmp = stack.Pop() / stack.Pop();
        if (tmp > 0)
        {
            stack.Push((int)Math.Floor(tmp));
        }
        else
        {
            stack.Push((int)Math.Ceiling(tmp));
        }
    }

    private static void Command10(Stack<int> stack)
    {
        if (stack.Pop() % 2 == 0)
        {
            stack.Push(1);
        }
    }

    private static void Command11(Stack<int> stack)
    {
        var a = stack.Pop();
        if (a % 2 != 0)
        {
            stack.Push(a);
        }
    }

    private static void Command12(Stack<int> stack)
    {
        var result = Math.Max(stack.Pop(), Math.Max(stack.Pop(), stack.Pop())); 
        stack.Push(result);
    }
}
