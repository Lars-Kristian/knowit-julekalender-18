﻿using System;
using System.Diagnostics;

namespace knowit_julekalender_18
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopWatch = Stopwatch.StartNew();

            var result = Day11.Compute();

            stopWatch.Stop();

            Console.WriteLine(result + " - " + stopWatch.ElapsedMilliseconds + "ms");
        
            var x = Console.Read();
        }
    }
}
