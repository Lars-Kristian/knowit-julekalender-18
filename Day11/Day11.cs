using System;
using System.IO;

public class Day11
{
    public static string Compute()
    {
        string fileInput = "";
        try
        {
            using (StreamReader sr = new StreamReader("Day11/input.txt"))
            {
                fileInput = sr.ReadToEnd();
            }
        }
        catch (Exception) { }

        var x = 0;
        var y = 0;

        for (var i = 0; i < fileInput.Length - 1; i += 2)
        {
            int length = fileInput[i] & 15;
            var direction = fileInput[i + 1];

            switch (direction)
            {
                case 'H': x += length; break;
                case 'V': x -= length; break;
                case 'F': y += length; break;
                case 'B': y -= length; break;
            }
        }

        return "[" + x + "," + y + "]";
    }
}